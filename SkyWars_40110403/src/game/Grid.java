package game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;

/*
 * The class that is built to represent the Grid the game requires to be played.
 * An instance of a MS, a list of ships on board, a 2d array of Squares and a size.
 * 
 * Getters and setters are implemented for all data plus extra methods for game functionality
 * which are explained individually below.
 */

public class Grid implements Serializable{
	private MasterShip ms = new MasterShip();
	private ArrayList<Ship> shipsOnBoard = new ArrayList<Ship>();
	private Square[][] grid;
	private int size;
	
	int total = 1;
	int rounds = 0;
	static Random randomGenerator = new Random();
	
	public Grid(int size) // Initialize grid
	{
		if(size <= 1 || size > 9)
		{
    		JOptionPane.showMessageDialog(null, "You have provided an invalid number and the grid is set to the default size: 4");
    		size = 4;
		}
		setGrid(new Square[size][size]);
		setSize(size);
		initializeGrid(size);
	}

	public Square[][] getGrid() {
		return grid;
	}

	public void setGrid(Square[][] grid) {
		this.grid = grid;
		this.updateGrid();
	}
	
	public void updateGrid()
	{
		for(int i = 0; i < size; i++)
		{
			for(int j = 0; j < size; j++)
			{
				grid[i][j].updateShips();
			}
		}
	}
	
	public MasterShip getMs() {
		return this.ms;
	}

	public void setMs(MasterShip ms) {
		this.ms = ms;
	}
	
	public ArrayList<Ship> getShipsOnBoard() {
		return shipsOnBoard;
	}

	public void addShipsOnBoard(Ship ship) {
		this.shipsOnBoard.add(ship);
	}
	
	public void setAllShips(ArrayList<Ship> ships)
	{
		this.shipsOnBoard = ships;
	}

	public void removeShipFromBoard(Ship s)
	{
		shipsOnBoard.remove(s);
	}
	

	public int getSize() {
		return size;
	}
	
	
	public void setSize(int size)
	{
		this.size = size;
	}
	
	//This method is used to assign Squares to each position in the array and also provide them with a refrence to the Grid.
	private void initializeGrid(int size)
	{
		for(int i = 0; i < size; i++)
		{
			for(int j = 0; j < size; j++)
			{
				grid[i][j] = new Square(i,j);
				grid[i][j].setGridRef(this);
			}
		}
	}

	//Triggers a method in the Square class in order to get the list of ships registered to that instance.
	public ArrayList<Ship> getShipsOnSquare(int x, int y)
	{
		try {
			return grid[x][y].getShips();
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			return null;
		}
	}
	
	//Method to return a list of the neighbouring squares provided x and y.
	public ArrayList<Square> getNeighboursFor(int x, int y)
	{
		ArrayList<Square> rList = new ArrayList<Square>();
		
		if(x < size && y < size)
		{
			for(int i = 0; i < size; i++)
			{
				for(int j = 0; j < size; j++)
				{
					if(i == x-1 || i == x  || i == x+1)
					{
						if(j == y-1 || j == y || j == y+1)
						{
							if(i != x || j !=y)
							{
								rList.add(grid[i][j]);
							}
						}				
					}
				}
			}	
		}
			
		return rList;
	}
	
	//A method that adds the MS to the board at a random position.
	public void initializeGame()
	{
		this.addShipsOnBoard(ms);
		int randomx, randomy;
		randomx = randomGenerator.nextInt(size-1);
		randomy = randomGenerator.nextInt(size-1);
		Square start = grid[randomx][randomy];
		start.move(ms);
	}
	
	//The two methods below are to loop through each ship on the board and move it while updating it's references.
	public void moveShips() {
		rounds++;
		for(Ship s : shipsOnBoard)
		{
			moveShip(s);
		}
	}
	
	public void moveShip(Ship s) 
	{
		int x = s.getPos().getX();
		int y = s.getPos().getY();
		ArrayList<Square> ls = getNeighboursFor(x,y);
		int nextStep = randomGenerator.nextInt(ls.size()-1);
		
		Square currentSquare = grid[x][y];
		currentSquare.removeShip(s);
		
		Square nextSquare = ls.get(nextStep);
		nextSquare.move(s);
	}
	
	//The method will add a ship to the board with a 1/3 chance.
	public void addShipsToGame() {
		int enter = randomGenerator.nextInt(2);
		if(enter == 1)
		{
			total++;
			EnemyShip newEnemy = EnemyShip.getShip();
			Square enemyStart = grid[0][0];
			enemyStart.move(newEnemy);
			addShipsOnBoard(newEnemy);
		}
	}
}
