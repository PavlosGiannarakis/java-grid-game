package game;
//Strategy pattern for Modes used by the MasterShip.
public interface Mode {

	public String mode();
}
