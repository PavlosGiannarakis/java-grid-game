package game;

import java.io.Serializable;

//Base class of all ships and contains only the essentials, reference to the current position and a type.

public class Ship implements Observer, Serializable{
	
	private Square pos;
	private String type;
	
	public Ship(String type) {
		setType(type);
	}

	public Square getPos() {
		return pos;
	}

	public void updatePosition(Square pos) {
		this.pos = pos;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
