package game;

import junit.framework.TestCase;

public class EnemyShipTest extends TestCase {
	//Test to see if factory pattern is working correctly.
	public void testGetShip() {
		EnemyShip x = EnemyShip.getShip();
		assertTrue(x instanceof BattleCruiser || x instanceof BattleShooter || x instanceof BattleStar);
	}

}
