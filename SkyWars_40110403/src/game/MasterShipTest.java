package game;

import junit.framework.TestCase;

public class MasterShipTest extends TestCase {

	//Tests to make sure the mode and hits are working correctly.
	public void testMode() {
		MasterShip ms = new MasterShip();
		ms.setMode(new Attack());
		assertTrue(ms.getMode() instanceof Attack);
	}
	
	public void testMode2() {
		MasterShip ms = new MasterShip();
		assertTrue(ms.getMode() instanceof Defence);
	}

	public void testHits() {
		MasterShip ms = new MasterShip();
		assertTrue(ms.getHits() == 2);
	}
	
	public void testHits2() {
		MasterShip ms = new MasterShip();
		ms.setMode(new Attack());
		assertTrue(ms.getHits() == 3);
	}
	
	//Testing conflict handler method.
	public void testConflict() {
		Grid ex = new Grid(4);
		ex.initializeGame();
		MasterShip ms = new MasterShip();
		EnemyShip es = EnemyShip.getShip();
		EnemyShip es2 = EnemyShip.getShip();
		
		ex.getGrid()[1][1].move(ms);
		ex.getGrid()[1][1].move(es);
		ex.getGrid()[1][1].move(es2);
		ms.conflictHandler();
		
		assertTrue(ms.gameOver);
	}
	
	public void testConflict2() {
		Grid ex = new Grid(4);
		ex.initializeGame();
		MasterShip ms = new MasterShip();
		EnemyShip es = EnemyShip.getShip();
		EnemyShip es2 = EnemyShip.getShip();
		ms.setMode(new Attack());
		
		ex.getGrid()[1][1].move(ms);
		ex.getGrid()[1][1].move(es);
		ex.getGrid()[1][1].move(es2);
		ms.conflictHandler();
		
		assertFalse(ms.gameOver);
	}
}
