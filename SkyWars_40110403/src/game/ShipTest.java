package game;

import junit.framework.TestCase;

public class ShipTest extends TestCase {

	public void testPos() {
		int x = 1, y = 2;
		Ship s = new MasterShip();
		s.updatePosition(new Square(x,y));
		assertTrue(s.getPos().getX() == x && s.getPos().getY() == y);
	}

	public void testType() {
		Ship s = new MasterShip();
		assertTrue(s.getType() == "MasterShip");
	}

}
