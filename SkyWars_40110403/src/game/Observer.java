package game;

//Methods required for observer objects, in this case any instance of class ship uses this interface.
public interface Observer {
	
	public void updatePosition(Square s);

}
