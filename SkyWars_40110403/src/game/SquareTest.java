package game;

import junit.framework.TestCase;

public class SquareTest extends TestCase {
	int x = 1, y = 1;
	public void testAddShips() {
		Square s = new Square(x,y);
		int ol = s.getShips().size();
		s.addShip(new Ship("Enemy"));
		int nl = s.getShips().size();
		assertTrue(ol+1 == nl);
	}
	
	public void testRemoveShips() {
		Square s = new Square(x,y);
		MasterShip ms = new MasterShip();
		s.addShip(new Ship("Enemy"));
		s.addShip(new Ship("Enemy"));
		s.addShip(ms);
		s.removeShip(ms);
		int len = s.getShips().size();
		assertTrue(len == 2);
	}
	
	public void testGridRef() {
		Grid ex = new Grid(4);
		Square s = new Square(x,y);
		s.setGridRef(ex);
		assertTrue(s.getGrid() == ex);
	}
	
	public void testXandY() {
		Square s = new Square(x,y);
		assertTrue(s.getX() == x && s.getY() == y);
	}

}
