package game;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(EnemyShipTest.class);
		suite.addTestSuite(GridTest.class);
		suite.addTestSuite(SquareTest.class);
		suite.addTestSuite(ShipTest.class);
		suite.addTestSuite(MasterShipTest.class);
		//$JUnit-END$
		return suite;
	}

}
