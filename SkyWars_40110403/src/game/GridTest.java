package game;

import java.util.ArrayList;
import junit.framework.TestCase;

public class GridTest extends TestCase {
	int size = 4;
	
	public void testGrid() {
		Grid t = new Grid(size);
		boolean flag = true;
		for(int i = 0; i < size; i++)
		{
			if(t.getGrid()[i].length != size) {
				flag = false; 
				break;
			} 
		}
		assertTrue(flag);
	}

	public void testSize() {
		Grid t = new Grid(size);
		assertTrue(size == t.getSize());
	}


	public void testGetNeighboursFor() {
		Grid t = new Grid(size);
		int ex_x = 1;
		int ex_y = 1;
		boolean flag = true;
		ArrayList<Square> ex = t.getNeighboursFor(ex_x, ex_y);
		for(int i = 0; i < ex.size(); i++)
		{
			if(ex.get(i).getX() > ex_x+1 || ex.get(i).getX() < ex_x-1 || ex.get(i).getY() > ex_y+1 || ex.get(i).getY() < ex_y-1) 
			{
				flag = false;
				break;
			}
		}
		assertTrue(flag);
	}
	
	public void testShipsOnBoard() {
		Grid t = new Grid(4);
		MasterShip ms = new MasterShip();
		
		t.addShipsOnBoard(new Ship("Enemy"));

		t.addShipsOnBoard(ms);

		t.removeShipFromBoard(ms);
		
		int len = t.getShipsOnBoard().size();
		assertTrue(len == 1);
	}

}
