package game;

import java.io.Serializable;
import java.util.ArrayList;
/*
 * The class that populates the 2d array in class Grid. Keeps a reference to the grid, a list of ships and it's x & y position on the board.
 */
public class Square implements Observable, Serializable {
	private int x;
	private int y;
	private ArrayList<Ship> ships = new ArrayList<Ship>();
	private Grid grid_ref;
	
	public Grid getGrid() {
		return grid_ref;
	}

	public void setGridRef(Grid grid_ref) {
		this.grid_ref = grid_ref;
	}

	public Square(int x, int y)
	{
		setX(x);
		setY(y);
		setShips(new ArrayList<Ship>());
	}
	
	public void setX(int tx)
	{
		this.x = tx;
	}
	
	public void setY(int ty)
	{
		this.y = ty;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public void addShip(Ship s)
	{
		ships.add(s);
	}
	
	public void removeShip(Ship s)
	{
		ships.remove(s);
	}

	public ArrayList<Ship> getShips() {
		return ships;
	}

	public void setShips(ArrayList<Ship> ships) {
		this.ships = ships;
	}
	
	//Update Square reference in the Ship class.
	public void updateShips()
	{
		for(Ship s : this.ships)
		{
			s.updatePosition(this);
		}
	}
	
	public void updateShip(Ship s) 
	{
		s.updatePosition(this);
	}
	
	public void move(Ship s)
	{
		this.addShip(s);
		this.updateShip(s);
	}
	
}
