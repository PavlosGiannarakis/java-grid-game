package game;

import java.util.ArrayList;

/* 	
 * Final class MasterShip which extends Ship and also implements the strategy pattern for Mode which mannipulates the hits allowed.
 * While also including a conflict handler method along with a toggle mode method.
 */
public final class MasterShip extends Ship {
	private int hits;
	protected Mode mode;
	//Count of total removed Ships from conflicts.
	int removed = 0;
	//Flag to show that the MasterShip has been eliminated from game.
	boolean gameOver = false;

	public MasterShip() {
		super("MasterShip");
		setMode(new Defence());
	}
	
	public Mode getMode() {
		return mode;
	}
	public void setMode(Mode mode) {
		this.mode = mode;
		if(mode instanceof Attack)
		{
			setHits(3);
		}
		else {
			setHits(2);
		}
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}
	
	/*
	 * The conflictHandler gets the instance of Square which the Ms is on and check the size of the list of ships on that Square.
	 * If more than one, meaning that Ms is not alone, then if more than hits allowed remove MS otherwise remove all other ships on Square.
	 */
	public void conflictHandler()
	{
		if(!gameOver)
		{
			Square current_pos = this.getPos();
			int sizeOL = current_pos.getShips().size();
			ArrayList<Ship> tbr = new ArrayList<Ship>();
			if(sizeOL > 1 && sizeOL <= hits)
			{
				for(Ship s : current_pos.getShips())
				{
					if(s instanceof EnemyShip)
					{
						removed++;
						tbr.add(s);
						current_pos.getGrid().removeShipFromBoard(s); // Could add to removeShip but breaks moveShip.
					}
				}
				System.out.println(tbr.size() + " removed.");
				for(Ship es : tbr)
				{
					current_pos.removeShip(es);
				}
			}
			else if(sizeOL > hits)
			{
				System.out.println("Game over.");
				removed++;
				gameOver = true;
				System.out.println(this.getType() + " removed.");
				current_pos.removeShip(this);
				current_pos.getGrid().removeShipFromBoard(this);
			}
			else 
			{
				System.out.println("No coflict.");
			}
		}
	}
	
	//Method to toggle mode.
	public void changeMode()
	{
		if(this.getMode() instanceof Defence)
		{
			System.out.println("AttackMode");
			this.setMode(new Attack());
		}
		else {
			System.out.println("DefenceMode");
			this.setMode(new Defence());
		}
	}
}
