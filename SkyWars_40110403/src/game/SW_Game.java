package game;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class SW_Game implements Serializable {
	//Changing the grid initial size will also affect the GUI display, making the game playable at any grid size.
	Grid grid;
	ArrayList<Grid> previousMoves = new ArrayList<Grid>();
	//Variable Used to store high score for game comparison.
	int highscore = 0;
	//Names of files used for storage.
	String fileName = "game.text";
	String scoreFile = "score.text";
	
	public SW_Game(int size)
	{
		grid = new Grid(size);
	}

	//Method used to serialise the game moves in order to make a deep copy of the Grid for the ArrayList, for the Undo function.
	public void saveMove(int pos)
	{
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(grid);
			oos.flush();
			oos.close();
			bos.close();
			byte[] byteData = bos.toByteArray();
			ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
			Grid deepCopyGrid = (Grid) new ObjectInputStream(bais).readObject();
			previousMoves.add(pos,deepCopyGrid);
		}catch(IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	//Method to save the high score which was achieved.
	public void saveHighScore()
	{
		FileOutputStream fos;
		ObjectOutputStream oos;
		try {
			fos = new FileOutputStream(scoreFile);
			oos = new ObjectOutputStream(fos);

			oos.writeObject(grid.rounds);
			oos.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
		System.out.println("Saved Highscore.");
	}
	
	//Method to retrieve stored high score.
	public void getHighScore() {
		FileInputStream fis;
		ObjectInputStream ois;
		
		try {
			fis = new FileInputStream(scoreFile);
			ois = new ObjectInputStream(fis);
			
			int storedScore = (int) ois.readObject();
			highscore = storedScore;
		}catch(IOException e) {
		} catch(ClassNotFoundException e) {
		}
	}
	
	//Method used to store copy of game for later use.
	public void saveGame()
	{
		FileOutputStream fos;
		ObjectOutputStream oos;
		try {
			fos = new FileOutputStream(fileName);
			oos = new ObjectOutputStream(fos);

			oos.writeObject(this);
			oos.close();
		}catch(IOException e) {
			e.printStackTrace();
		}

		System.out.println("Saved.");
	}
	
	//Method used to restore game to board from stored file.
	public void reloadGame() 
	{
		FileInputStream fis;
		ObjectInputStream ois;
		
		try {
			fis = new FileInputStream(fileName);
			ois = new ObjectInputStream(fis);
			
			SW_Game after = (SW_Game) ois.readObject();
			
			grid.setGrid(after.grid.getGrid());
			grid.setAllShips(after.grid.getShipsOnBoard());
			grid.setMs(after.grid.getMs());
			grid.total = after.grid.total;
			grid.rounds = after.grid.rounds;
			previousMoves.clear();
			previousMoves.addAll(after.previousMoves);
    		JOptionPane.showMessageDialog(null, "Game Reloaded");
		}catch(IOException e) {
		} catch(ClassNotFoundException e) {
		}
	}
	
	//Method for moving and undoing, act as wrappers for methods in child classes.
	public void move() {
		saveMove(grid.rounds);
		grid.moveShips();
		grid.addShipsToGame();	
		grid.getMs().conflictHandler();
	}
	
	public void undoMove() {
		grid.rounds--;
		Grid after = previousMoves.get(grid.rounds);
		grid.setGrid(after.getGrid());
		grid.setAllShips(after.getShipsOnBoard());
		grid.setMs(after.getMs());
	}
}
