package game;

import java.util.Random;

public class EnemyShip extends Ship {
	/*
	 * Each enum represents one of the existing types of enemy ship.
	 * The untaught factory pattern is used to get a random instance of an enemyship.
	 * If you want to extend it, a new enum has to be added and a if statement created to handle the new enum below.
	 * Therefore it is easy to extend with two-three lines of code.
	 */
	enum shipType {BC, BS1, BS2};
	public EnemyShip(String type) {
		super(type);
	}
	
	//Factory Pattern Technique - For the untaught marks.
	public static EnemyShip getShip(){
		Random randomGenerator = new Random();
		int type = randomGenerator.nextInt(shipType.values().length - 1);	
		if(shipType.values()[type].equals(shipType.BC)){
			return new BattleCruiser();

		} else if(shipType.values()[type].equals(shipType.BS1)){
			return new BattleStar();

		} else if(shipType.values()[type].equals(shipType.BS2)){
			return new BattleShooter();
		}

		return null;
	}

}
