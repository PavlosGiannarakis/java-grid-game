package game;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI {
	//Parts of GUI required
	private JFrame frame;
	private SW_Game game;
	private JPanel panel;
	private JLabel lblScoreLable;
	private JLabel lblHScoreLable;
	private JLabel[][] grid;
	private int size;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * Initialize an instance of the game and get the MasterShips position coordinates and then create the display of the board.
	 */
	public GUI() {
		//Size has to be bigger than 1 and smaller than 10 or else it will be corrected to the default which is 4.
		game = new SW_Game(4);
		game.grid.initializeGame();
		size = game.grid.getSize();
		int msX = game.grid.getMs().getPos().getX();
		int msY = game.grid.getMs().getPos().getY();
		initialize(msX,msY);
	}

	/**
	 * Initialize the contents of the frame.
	 * Lays out the interface along with the buttons and how they should be handled.
	 */
	private void initialize(int x, int y) {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Sky wars GUI");
	    panel = new JPanel();
	    //Adjusts gui layout according to board size.
	    if(size < 4) {
	    	panel.setLayout(new GridLayout(size + 2 + (4-size), size));	
	    }
	    else{
	    	panel.setLayout(new GridLayout(size + 2, size));
	    }
	    frame.getContentPane().add(panel);
	    
	    //Grid initialisation
		grid= new JLabel[size][size];
	    for (int i = 0; i < size; i++){
	        for (int j = 0; j < size; j++){
	        	if(i == x && j == y)
	        	{
	        		grid[i][j] = new JLabel("MasterShip");
	        	} else grid[i][j] = new JLabel("");
	            grid[i][j].setBorder(new LineBorder(Color.BLACK));
	            grid[i][j].setOpaque(true);
	            panel.add(grid[i][j]);
	        }
	    }
	    
	    //Button definitions and the functionality on click are added to the board.
	    JButton btnUndo = new JButton("Undo");
	    btnUndo.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		if(game.grid.rounds > 0)
	    		{
	    			System.out.println("Undo");
					game.undoMove();
					System.out.println(game.grid.getMs().getMode().mode());
		    		updateBoard();
		    		lblScoreLable.setText("Score: "+ game.grid.rounds);
	    		}
	    		else JOptionPane.showMessageDialog(null, "Can't Undo any further.");
	    	}
	    });
	    panel.add(btnUndo);
	    
	    JButton btnMove = new JButton("Move");
	    btnMove.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		//Check if the game is over before moving.
				if(game.grid.getMs().gameOver)
				{
					StringBuilder message = new StringBuilder("Game Over\n"+ "You survived " + game.grid.rounds + " round(s) and destroyed "+
				(game.grid.getMs().removed-1) + " Enemy Ships.");
					if(game.grid.rounds > game.highscore)
					{
						message.append("\nThat is a new HighScore!");
						lblHScoreLable.setText("HighScore: "+ game.grid.rounds);
						game.saveHighScore();
					}
					JOptionPane.showMessageDialog(null, message.toString());
				}else {
					game.move();
					updateBoard();
		    		lblScoreLable.setText("Score: "+ game.grid.rounds);
	    		}
	    	}
	    });
	    panel.add(btnMove);
	    
	    JButton btnCM = new JButton("Change Mode");
	    btnCM.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		game.grid.getMs().changeMode();
	    		String mode = game.grid.getMs().getMode().mode();
	    		JOptionPane.showMessageDialog(null, "Changed mode to: " + mode);
	    	}
	    });
	    panel.add(btnCM);
	    
	    
	    JButton btnStart = new JButton("Start new");
	    btnStart.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		game = new SW_Game(game.grid.getSize());
	    		game.grid.initializeGame();
	    		lblScoreLable.setText("Score: "+ game.grid.rounds);
	    		updateBoard();
	    	}
	    });
	    panel.add(btnStart);
	    
	    JButton btnSave = new JButton("Save");
	    btnSave.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		game.saveGame();
	    		JOptionPane.showMessageDialog(null, "Game Saved");
	    	}
	    });
	    panel.add(btnSave);
	    
	    JButton btnReload = new JButton("Reload");
	    btnReload.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		game.reloadGame();
	    		updateBoard();
	    		lblScoreLable.setText("Score: "+ game.grid.rounds);
	    	}
	    });
	    panel.add(btnReload);
	    
	    //Get highscore and add labels for score tracking
	    game.getHighScore();
	    
	    lblScoreLable = new JLabel("Score: "+ game.grid.rounds);
	    panel.add(lblScoreLable);
	    
	    lblHScoreLable = new JLabel("HighScore: "+ game.highscore);
	    panel.add(lblHScoreLable);  
	}
	
	//Method responsible for getting the list of ships on each square and creating the new label text for that square in the GUI.
	public void updateBoard()
	{
	    for (int i = 0; i < size; i++){
	        for (int j = 0; j < size; j++){
	        	int shipsSize = game.grid.getGrid()[i][j].getShips().size();
	        	if(shipsSize > 0)
	        	{
	        		StringBuilder newLableText = new StringBuilder("<html>");
	        		for(Ship s : game.grid.getGrid()[i][j].getShips())
	        		{
		        		newLableText.append(s.getType() + "<br/>");
	        		}
	        		newLableText.append("</html>");
	        		grid[i][j].setText(newLableText.toString());
	        	}
	        	else grid[i][j].setText("");
	        }
	    }
	}

}
