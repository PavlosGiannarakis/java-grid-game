package game;

//Methods required for the observable objects, Sqaure class in this case. 
public interface Observable {
	public void addShip(Ship s);
	public void removeShip(Ship s);
	public void updateShips();
}
